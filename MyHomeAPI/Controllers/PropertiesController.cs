﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MyHomeAPI.Data;
using System.Threading.Tasks;
using MyHomeAPI.Models;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace MyHomeAPI.Controllers
{
    //[Authorize(Roles = ("Agent"))]
    [Route("properties")]
    public class PropertiesController : Controller
    {
        MyHomeDataContext _context;

        public PropertiesController(MyHomeDataContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("all")]
        public async Task<IEnumerable<Property>> GetAllProperties()
        {
            return await _context.Properties.Include(x => x.Address).Include(x => x.Agent).Include(x => x.Customer).ToListAsync();
        }

        //public IActionResult Index()
        //{
        //    var user = _context.AppUsers.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
        //    return View(_context.Agents.Where(x=> x.Id == user.Id).FirstOrDefault());
        //}
    }
}