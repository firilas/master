﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MyHomeAPI.Data;
using System.Threading.Tasks;
using MyHomeAPI.Models;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace MyHomeAPI.Controllers
{
    //[Authorize(Roles = ("Agent"))]
    [Route("agents")]
    public class AgentsController : Controller
    {
        MyHomeDataContext _context;

        public AgentsController(MyHomeDataContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("all")]
        public async Task<IEnumerable<Agent>> GetAllAgents()
        {
            return await _context.Agents.Include(x => x.AppUser).ToListAsync();
        }

        //public IActionResult Index()
        //{
        //    var user = _context.AppUsers.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
        //    return View(_context.Agents.Where(x=> x.Id == user.Id).FirstOrDefault());
        //}
    }
}