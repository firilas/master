﻿using System.ComponentModel.DataAnnotations;

namespace MyHomeAPI.Models
{
    public class Location
    {
        [Key]
        public string Id { set; get; }
        public string LocationURL { set; get; }
        public string Latitude { set; get; }
        public string Longitude { set; get; }
    }
}
