﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyHomeAPI.Models
{
    public class Company
    {
        [Key]
        public string Id { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        [ForeignKey("Address")]
        public string AddressId { set; get; }
        public Address Address { set; get; }
    }
}
