﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyHomeAPI.Models
{
    public class Property
    {
        [Key]
        public string Id { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        public int Rooms { set; get; }
        [ForeignKey("Address")]
        public string AddressId { set; get; }
        public Address Address { set; get; }
        [ForeignKey("Agent")]
        public string AgentId { set; get; }
        public Agent Agent { set; get; }
        [ForeignKey("Customer")]
        public string CustomerId { set; get; }
        public Customer Customer { set; get; }

    }
}
