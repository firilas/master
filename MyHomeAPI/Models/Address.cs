﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyHomeAPI.Models
{
    public class Address
    {
        [Key]
        public string Id { set; get; }
        public string Street { set; get; }
        [ForeignKey("Location")]
        public string LocationId { set; get; }
        public Location Location { set; get; }
    }
}
