﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyHomeAPI.Models
{
    public class AppUser : IdentityUser
    {
        public DateTime CreatedDate { set; get; }

        [ForeignKey("Address")]
        public string AddressId { set; get; }
        public Address Address { set; get; }
    }
}
