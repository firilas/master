﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyHomeAPI.Models
{
    public class Agent
    {
        [Key]
        public string Id { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        [ForeignKey("Company")]
        public string CompanyId { set; get; }
        public Company Company { set; get; }
        [ForeignKey("AppUser")]
        public string AppUserId { set; get; }
        public AppUser AppUser { set; get; }
    }
}
