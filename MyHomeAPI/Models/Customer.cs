﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyHomeAPI.Models
{
    public class Customer
    {
        [Key]
        public string Id { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        [ForeignKey("Agent")]
        public string AgentId { set; get; }
        public Agent Agent { set; get; }

        [ForeignKey("AppUser")]
        public string AppUserId { set; get; }
        public AppUser AppUser { set; get; }
    }
}
