﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using MyHomeAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyHomeAPI.Data
{
    public static class DbInitializer
    {
        public static void Initialize(MyHomeDataContext context)
        {
            context.Database.EnsureCreated();

            if (!context.Roles.Any())
            {
                var roles = new IdentityRole[]
                {
                    new IdentityRole{Name = "Admin", NormalizedName = "Admin"},
                    new IdentityRole{Name = "Agent", NormalizedName = "Agent"},
                    new IdentityRole{Name = "Manager", NormalizedName = "Manager"},
                    new IdentityRole{Name = "Customer", NormalizedName = "Customer"},
                    new IdentityRole{Name = "StandarUser", NormalizedName = "StandarUser"}
                };
                foreach (IdentityRole role in roles)
                {
                    context.Roles.Add(role);
                }
                context.SaveChanges();
            }

            if (!context.Locations.Any())
            {
                var locations = new Location[]
                {
                new Location{Id="1",Latitude="35.158539", Longitude="33.317241", LocationURL="https://www.google.com/maps/place/35.158566,+33.317284" },
                new Location{Id="2",Latitude="35.158539", Longitude="33.317241", LocationURL="https://www.google.com/maps/place/35.158566,+33.317284" },
                new Location{Id="3",Latitude="35.158539", Longitude="33.317241", LocationURL="https://www.google.com/maps/place/35.158566,+33.317284" },
                new Location{Id="4",Latitude="35.158539", Longitude="33.317241", LocationURL="https://www.google.com/maps/place/35.158566,+33.317284" },
                new Location{Id="5",Latitude="35.158539", Longitude="33.317241", LocationURL="https://www.google.com/maps/place/35.158566,+33.317284" },
                new Location{Id="6",Latitude="35.158539", Longitude="33.317241", LocationURL="https://www.google.com/maps/place/35.158566,+33.317284" },
                new Location{Id="7",Latitude="35.158539", Longitude="33.317241", LocationURL="https://www.google.com/maps/place/35.158566,+33.317284" },
                new Location{Id="8",Latitude="35.158539", Longitude="33.317241", LocationURL="https://www.google.com/maps/place/35.158566,+33.317284" }
                };
                foreach (Location l in locations)
                {
                    context.Locations.Add(l);
                }
                context.SaveChanges();
            }

            if (!context.Addresses.Any())
            {
                var addresses = new Address[]
                {
                    new Address{Id="1",LocationId="1",Street="Aftokratoros Ioustinianou 6"},
                    new Address{Id="2",LocationId="2",Street="Aftokratoros Ioustinianou 6"},
                    new Address{Id="3",LocationId="3",Street="Aftokratoros Ioustinianou 6"},
                    new Address{Id="4",LocationId="4",Street="Aftokratoros Ioustinianou 6"},
                    new Address{Id="5",LocationId="5",Street="Aftokratoros Ioustinianou 6"},
                    new Address{Id="6",LocationId="6",Street="Aftokratoros Ioustinianou 6"},
                    new Address{Id="7",LocationId="7",Street="Aftokratoros Ioustinianou 6"},
                    new Address{Id="8",LocationId="8",Street="Aftokratoros Ioustinianou 6"},
                    new Address{Id="9",LocationId="8",Street="Company street"}
                };

                foreach (Address a in addresses)
                {
                    context.Addresses.Add(a);
                }
                context.SaveChanges();
            }

            if (!context.Companies.Any())
            {
                var companies = new Company[]
                {
                    new Company{Id="1", AddressId="9", Name="Company 1", Description="Short Description for Company 1" },
                    new Company{Id="2", AddressId="9", Name="Company 2", Description="Short Description for Company 2" },
                    new Company{Id="3", AddressId="9", Name="Company 3", Description="Short Description for Company 3" },
                    new Company{Id="4", AddressId="9", Name="Company 4", Description="Short Description for Company 4" }
                };
                foreach (Company c in companies)
                {
                    context.Companies.Add(c);
                }
                context.SaveChanges();
            }

            if (!context.Agents.Any())
            {
                var agents = new Agent[]
                {
                    new Agent{Id="1", FirstName="Carson",   LastName="Alexander"},
                    new Agent{Id="2", FirstName="Meredith", LastName="Alonso"},
                    new Agent{Id="3", FirstName="Arturo",   LastName="Anand"},
                    new Agent{Id="4", FirstName="Gytis",    LastName="Barzdukas"},
                    new Agent{Id="5", FirstName="Yan",      LastName="Li"},
                    new Agent{Id="6", FirstName="Peggy",    LastName="Justice"},
                    new Agent{Id="7", FirstName="Laura",    LastName="Norman"},
                    new Agent{Id="8", FirstName="Nino",     LastName="Olivetto"}
                };
                foreach (Agent a in agents)
                {
                    AppUser user = new AppUser { UserName = "demo" + a.Id };
                    context.AppUsers.Add(user);
                    a.AppUserId = user.Id;
                    context.Agents.Add(a);
                }
                context.SaveChanges();
            }

            if (!context.UserRoles.Any())
            {
                IdentityRole role = context.Roles.Where(x => x.Name == "Agent").FirstOrDefault();
                
                foreach (AppUser user in context.AppUsers)
                {
                    context.UserRoles.Add(new IdentityUserRole<string> { RoleId = role.Id, UserId = user.Id });
                }
                context.SaveChanges();
            }

            if (!context.Customers.Any())
            {
                var customers = new Customer[]
                {
                    new Customer{Id="1", FirstName="Customer name 1",   LastName="Alexander"},
                    new Customer{Id="2", FirstName="Customer name 2",   LastName="Alonso"},
                    new Customer{Id="3", FirstName="Customer name 3",   LastName="Anand"},
                    new Customer{Id="4", FirstName="Customer name 4",   LastName="Barzdukas"},
                    new Customer{Id="5", FirstName="Customer name 5",   LastName="Li"},
                    new Customer{Id="6", FirstName="Customer name 6",   LastName="Justice"},
                    new Customer{Id="7", FirstName="Customer name 7",   LastName="Norman"},
                    new Customer{Id="8", FirstName="Customer name 8",   LastName="Olivetto"}
                };
                foreach (Customer cu in customers)
                {
                    AppUser user = new AppUser { UserName = "demo" + cu.Id + cu.Id };
                    context.AppUsers.Add(user);
                    cu.AppUserId = user.Id;
                    context.Customers.Add(cu);
                }
                context.SaveChanges();
            }

            if (!context.Properties.Any())
            {
                var properties = new Property[]
                {
                    new Property{Id="1",  AddressId="1", Name="Property Name 1",  Description="Short Property Desctription for Asset 1",  Rooms=2},
                    new Property{Id="2",  AddressId="2", Name="Property Name 2",  Description="Short Property Desctription for Asset 2",  Rooms=1},
                    new Property{Id="3",  AddressId="3", Name="Property Name 3",  Description="Short Property Desctription for Asset 3",  Rooms=3},
                    new Property{Id="4",  AddressId="4", Name="Property Name 4",  Description="Short Property Desctription for Asset 4",  Rooms=4},
                    new Property{Id="5",  AddressId="5", Name="Property Name 5",  Description="Short Property Desctription for Asset 5",  Rooms=3},
                    new Property{Id="6",  AddressId="6", Name="Property Name 6",  Description="Short Property Desctription for Asset 6",  Rooms=2},
                    new Property{Id="7",  AddressId="7", Name="Property Name 7",  Description="Short Property Desctription for Asset 7",  Rooms=2},
                    new Property{Id="8",  AddressId="8", Name="Property Name 8",  Description="Short Property Desctription for Asset 8",  Rooms=2},
                    new Property{Id="9",  AddressId="2", Name="Property Name 9",  Description="Short Property Desctription for Asset 9",  Rooms=1},
                    new Property{Id="10", AddressId="3", Name="Property Name 10", Description="Short Property Desctription for Asset 10", Rooms=1},
                    new Property{Id="11", AddressId="4", Name="Property Name 11", Description="Short Property Desctription for Asset 11", Rooms=2},
                    new Property{Id="12", AddressId="5", Name="Property Name 12", Description="Short Property Desctription for Asset 12", Rooms=2},
                    new Property{Id="13", AddressId="6", Name="Property Name 13", Description="Short Property Desctription for Asset 13", Rooms=1},
                    new Property{Id="14", AddressId="7", Name="Property Name 14", Description="Short Property Desctription for Asset 14", Rooms=2},
                    new Property{Id="15", AddressId="8", Name="Property Name 15", Description="Short Property Desctription for Asset 15", Rooms=5}
                };
                foreach (Property pr in properties)
                {
                    context.Properties.Add(pr);
                }
                context.SaveChanges();
            }
        }
    }
}
