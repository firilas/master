﻿using MyHomeAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace MyHomeAPI.Data
{
    public class MyHomeDataContext : IdentityDbContext<AppUser>
    {
        public MyHomeDataContext(DbContextOptions<MyHomeDataContext> options) 
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            base.OnModelCreating(builder);

            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public DbSet<Property> Properties { get; set; }
        public DbSet<AppUser> AppUsers { get; set; }
        public DbSet<Agent> Agents { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Location> Locations { get; set; }
    }
}
